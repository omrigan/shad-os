#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stdint.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <math.h>

extern size_t page_size;
extern double *sqrts;

void calculate_sqrts(double *sqrt_pos, int start, int nr);

// align_down - rounds a value down to an alignment
// @x: the value
// @a: the alignment (must be power of 2)
//
// Returns an aligned value.
#define align_down(x, a) ((x) & ~((typeof(x))(a) - 1))

void
handle_sigsegv(int sig, siginfo_t *si, void *ctx)
{
  // Your code here.

  // replace these three lines with your implementation
  uintptr_t fault_addr = (uintptr_t)si->si_addr;
  printf("oops got SIGSEGV at 0x%lx\n", fault_addr);
  exit(EXIT_FAILURE);
}


